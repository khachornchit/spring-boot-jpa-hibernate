package net.putfirstthingsfirst.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import net.putfirstthingsfirst.model.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {
	public List<Employee> findByDepartmentId(long departmentId);
}
