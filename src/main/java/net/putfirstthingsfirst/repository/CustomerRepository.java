package net.putfirstthingsfirst.repository;

import org.springframework.data.repository.CrudRepository;

import net.putfirstthingsfirst.model.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Long> {

}
