package net.putfirstthingsfirst.repository;

import org.springframework.data.repository.CrudRepository;

import net.putfirstthingsfirst.model.Department;

public interface DepartmentRepository extends CrudRepository<Department, Long> {

}
