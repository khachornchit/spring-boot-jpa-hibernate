package net.putfirstthingsfirst.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import net.putfirstthingsfirst.model.Employee;
import net.putfirstthingsfirst.service.DepartmentService;
import net.putfirstthingsfirst.service.EmployeeService;

@RestController
public class EmployeeController {

	@Autowired
	private DepartmentService departmentService;

	@Autowired
	private EmployeeService employeeService;

	@RequestMapping("/departments/{departmentId}/employees")
	public List<Employee> getAll(@PathVariable long departmentId) {
		return employeeService.getByDepartmentId(departmentId);
	}

	@RequestMapping("/departments/{customerId}/employees/{employeeId}")
	public Employee get(@PathVariable long customerId, @PathVariable long employeeId) {
		return employeeService.get(employeeId);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/departments/{customerId}/employees")
	public Employee add(@RequestBody Employee employee, @PathVariable long customerId) {
		employee.setDepartment(departmentService.get(customerId));
		return employeeService.add(employee);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/departments/{customerId}/employees/{employeeId}")
	public Employee update(@RequestBody Employee employee, @PathVariable long customerId,
			@PathVariable long employeeId) {
		return employeeService.update(employeeId, employee);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/departments/{customerId}/employees/{employeeId}")
	public void delete(@PathVariable long customerId, @PathVariable long employeeId) {
		employeeService.delete(employeeId);
	}
}
