package net.putfirstthingsfirst.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import net.putfirstthingsfirst.model.Department;
import net.putfirstthingsfirst.service.DepartmentService;

@RestController
public class DepartmentController {

	@Autowired
	private DepartmentService departmentService;

	@RequestMapping("/departments")
	public List<Department> get() {
		return departmentService.get();
	}

	@RequestMapping("/departments/{id}")
	public Department get(@PathVariable long id) {
		return departmentService.get(id);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/departments")
	public Department add(@RequestBody Department department) {
		return departmentService.add(department);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/departments/{id}")
	public Department update(@RequestBody Department department, @PathVariable long id) {
		return departmentService.update(id, department);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/departments/{id}")
	public void delete(@PathVariable long id) {
		departmentService.delete(id);
	}
}
