package net.putfirstthingsfirst.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AppController {
	
	@RequestMapping("/")
	public String greeting() {
		return "Greeting from spring boot development project.";
	}
}