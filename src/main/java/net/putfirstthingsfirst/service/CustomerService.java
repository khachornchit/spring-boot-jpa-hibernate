package net.putfirstthingsfirst.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.putfirstthingsfirst.model.Customer;
import net.putfirstthingsfirst.repository.CustomerRepository;

@Service
public class CustomerService {

	@Autowired
	private CustomerRepository customerRepository;

	public List<Customer> get() {
		List<Customer> customers = new LinkedList<Customer>();
		customerRepository.findAll().forEach(x -> customers.add(x));
		return customers;
	}

	public Customer get(long id) {
		return customerRepository.findById(id).get();
	}

	public Customer add(Customer customer) {
		customerRepository.save(customer);
		return customer;
	}

	public Customer update(long id, Customer customer) {
		customerRepository.save(customer);
		return customer;
	}

	public void delete(long id) {
		customerRepository.deleteById(id);
	}
}
