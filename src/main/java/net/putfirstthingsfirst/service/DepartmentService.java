package net.putfirstthingsfirst.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.putfirstthingsfirst.model.Department;
import net.putfirstthingsfirst.repository.DepartmentRepository;

@Service
public class DepartmentService {

	@Autowired
	private DepartmentRepository departmentRepository;

	public List<Department> get() {
		List<Department> departments = new LinkedList<Department>();
		departmentRepository.findAll().forEach(x -> departments.add(x));
		return departments;
	}

	public Department get(long id) {
		return departmentRepository.findById(id).get();
	}

	public Department add(Department department) {
		departmentRepository.save(department);
		return department;
	}

	public Department update(long id, Department department) {
		departmentRepository.save(department);
		return department;
	}

	public void delete(long id) {
		departmentRepository.deleteById(id);
	}
	
	
}
