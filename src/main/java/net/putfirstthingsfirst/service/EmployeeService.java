package net.putfirstthingsfirst.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.putfirstthingsfirst.model.Employee;
import net.putfirstthingsfirst.repository.EmployeeRepository;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;

	public List<Employee> getByDepartmentId(long departmentId) {
		List<Employee> employees = new LinkedList<Employee>();
		employeeRepository.findByDepartmentId(departmentId).forEach(x -> employees.add(x));
		return employees;
	}

	public Employee get(long employeeId) {
		return employeeRepository.findById(employeeId).get();
	}

	public Employee add(Employee employee) {
		employeeRepository.save(employee);
		return employee;
	}

	public Employee update(long id, Employee employee) {
		employeeRepository.save(employee);
		return employee;
	}

	public void delete(long id) {
		employeeRepository.deleteById(id);
	}
}
